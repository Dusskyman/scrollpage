import 'package:flutter/material.dart';
import 'package:scrollable_page/models/workers.dart';
import 'package:scrollable_page/widgets/worker_card.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(),
      home: ScrollablePage(),
    );
  }
}

class ScrollablePage extends StatelessWidget {
  final List<Worker> _workers = [
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Artem',
      surname: 'Davidov',
      position: 'Developer',
      id: 488,
    ),
    Worker(
      name: 'Egor',
      surname: 'Pavlov',
      position: 'CR',
      id: 364,
    ),
    Worker(
      name: 'Alisa',
      surname: 'Lisa',
      position: 'Team lead',
      id: 123,
    ),
    Worker(
      name: 'Oleg',
      surname: 'Nemchenko',
      position: 'QA',
      id: 565,
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: _workers.map((e) => WorkerCard(worker: e)).toList(),
      ),
    );
  }
}
