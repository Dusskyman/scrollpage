import 'package:flutter/material.dart';
import 'package:scrollable_page/models/workers.dart';

class WorkerCard extends StatelessWidget {
  final Worker worker;

  WorkerCard({@required this.worker});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 70,
      child: Card(
        color: Colors.blue,
        elevation: 10,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Name: ${worker.name}',
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  'Surname: ${worker.surname}',
                  style: TextStyle(fontSize: 20),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Position: ${worker.position}',
                  style: TextStyle(fontSize: 15),
                ),
                Text(
                  'ID: ${worker.id}',
                  style: TextStyle(color: Colors.grey[400]),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
