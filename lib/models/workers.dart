import 'package:flutter/foundation.dart';

class Worker {
  String name;
  String surname;
  int id;
  String position;

  Worker({
    @required this.name,
    @required this.surname,
    @required this.position,
    @required this.id,
  });
}
